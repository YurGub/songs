import { ISong } from '../store/models/song';

interface ISongsResponse {
    status: number;
    data: ISong[];
    errorText?: string;
}

const songsArray = [{
    id: 1,
    band: 'Tool',
    name: 'Fear Inoculum',
    followStatus: 1,
},
{
    id: 3,
    band: 'Tool',
    name: 'Pneuma',
    followStatus: 0,
},
{
    id: 9,
    band: 'Tool',
    name: 'Invincible',
    followStatus: 0,
},
{
    id: 2,
    band: 'Tool',
    name: 'Descending',
    followStatus: 0,
},
{
    id: 4,
    band: 'Tool',
    name: '7mpest',
    followStatus: 0,
}];

export const getSongs = (): Promise<ISongsResponse> => {
    const promise = new Promise<ISongsResponse>(resolve => {
        resolve({
            status: 200,
            data: songsArray,
        })
    });

    return promise
}