declare module "MyTypes" {
    import { StateType, ActionType } from "typesafe-actions";
    export type ReducerState = StateType<typeof import("../store//reducers").default>;
    export type RootAction = ActionType<typeof import("../store//actions/actions")>;
}
