import * as React from "react";
import SongContainer from "./store/containers/SongContainer";
import './App.css';

export const App: React.FC<{}> = () => {
    return (
        <>
            <SongContainer />
        </>
    );
};

export default App
