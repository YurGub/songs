import { combineReducers } from "redux";
import { songReducer } from "./reducer";

const rootReducer = combineReducers({
    songs: songReducer
});

export default rootReducer;
