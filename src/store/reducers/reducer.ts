import * as MyTypes from "MyTypes";
import { actionTypes } from "../actions";
import { ISong } from '../models/song';

interface ISongModel {
    list: ISong[];
}

export const initialState: ISongModel = {
    list: [{
        id: 1,
        band: 'Tool',
        name: 'Fear Inoculum',
        followStatus: 1,
    },
    {
        id: 3,
        band: 'Tool',
        name: 'Pneuma',
        followStatus: 0,
    },
    {
        id: 9,
        band: 'Tool',
        name: 'Invincible',
        followStatus: 0,
    },
    {
        id: 2,
        band: 'Tool',
        name: 'Descending',
        followStatus: 0,
    },
    {
        id: 4,
        band: 'Tool',
        name: '7mpest',
        followStatus: 0,
    }]
};

export const songReducer = (state: ISongModel = initialState, action: MyTypes.RootAction) => {
    switch (action.type) {
    case actionTypes.TOGGLE_STATUS: {
        const songsList = [...state.list];
        songsList[action.payload].followStatus = songsList[action.payload].followStatus ? 0 : 1;

        return {
            ...state,
            list: songsList
        };
    }
    default:
        return state;
    }
};

export default songReducer
