import { action } from "typesafe-actions";

export enum actionTypes {
    TOGGLE_STATUS = "TOGGLE_STATUS"
}

export const todoActions = {
    toggleStatus: (idx: number) => action(actionTypes.TOGGLE_STATUS, idx)
};
