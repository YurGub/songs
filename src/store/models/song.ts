export interface ISong {
    id: number;
    band: string;
    name: string;
    followStatus: number;
}
