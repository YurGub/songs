import * as React from 'react';
import { getSongs } from '../../api/songs'
import { SongItem } from '../../components/SongItem'
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as MyTypes from "MyTypes";
import { actionTypes } from "../actions";
import { ISong } from '../models/song';

interface SongContainerProps {
    songsList: ISong[];
    toggleSongStatus: (idx: number) => object;
}

class SongContainer extends React.Component<SongContainerProps> {
    constructor(props:any) {
        super(props);
    }

    handleToggleStatus = (idx: number) => {
        console.log("deleting", idx);
        this.props.toggleSongStatus(idx);
    };

    render () {
        return (
            <table className="songs-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Песня</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.songsList.map((item, idx) => (
                        <SongItem data={item} key={idx} idx={idx} handleToggle={this.handleToggleStatus}/>
                    ))}
                </tbody>
            </table>
        )
    }
}

const MapStateToProps = (store: MyTypes.ReducerState) => {
    return {
        songsList: store.songs.list
    };
};
  
const MapDispatchToProps = (dispatch: Dispatch<MyTypes.RootAction>) => ({
    toggleSongStatus: (idx: number) => dispatch({ type: actionTypes.TOGGLE_STATUS, payload: idx })
});
  
export default connect(
    MapStateToProps,
    MapDispatchToProps
)(SongContainer);
