import * as React from 'react'
import { ISong } from '../store/models/song';

interface ISongsItemProps {
    data: ISong;
    idx: number;
    handleToggle: (idx: number) => void;
}


const SongItem: React.FC<ISongsItemProps> = props => {
    /*const [status, setStatus] = React.useState(followStatus);*/
    return (
        
    <>
        <tr data-id={ props.data.id } onClick={() => props.handleToggle(props.idx)}>
            <td>{ props.data.id }</td>
            <td>
                <span>{ props.data.band } - { props.data.name}</span>
                { props.data.followStatus ? <a title="В списке отслеживаемых" className="follow-icon"></a> : '' }
            </td>
        </tr>
    </>
    );
}

export { SongItem }
